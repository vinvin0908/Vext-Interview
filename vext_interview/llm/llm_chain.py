from langchain.chains import LLMChain
from langchain.llms import Cohere
from langchain.prompts import (
    ChatPromptTemplate,
    HumanMessagePromptTemplate,
    MessagesPlaceholder,
    SystemMessagePromptTemplate,
)
from langchain.memory import ConversationBufferMemory

default_prompt = "You are geologist, volcano specialist."


class LLModel:
    def __init__(self):
        self.llm = Cohere(cohere_api_key="DTwV9xX1ce0k2jXy3HrC7TfoxVRMFxZr3kURv5TV")
        self.prompt = ChatPromptTemplate(
            messages=[
                SystemMessagePromptTemplate.from_template(default_prompt),
                MessagesPlaceholder(variable_name="chat_history"),
                HumanMessagePromptTemplate.from_template("{question}"),
            ]
        )
        self.current_prompt = default_prompt
        self.memory = ConversationBufferMemory(
            memory_key="chat_history", return_messages=True
        )
        self.conversation = LLMChain(
            llm=self.llm, prompt=self.prompt, verbose=True, memory=self.memory
        )
        pass

    def update_prompt(self, new_prompt):
        self.prompt = ChatPromptTemplate(
            messages=[
                SystemMessagePromptTemplate.from_template(new_prompt),
                MessagesPlaceholder(variable_name="chat_history"),
                HumanMessagePromptTemplate.from_template("{question}"),
            ]
        )
        self.conversation = LLMChain(
            llm=self.llm, prompt=self.prompt, verbose=True, memory=self.memory
        )
        self.current_prompt = new_prompt

    def get_current_prompt(self):
        return self.current_prompt
