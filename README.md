# ChatBot for Vext

Used Django, Langchain, Cohere

Built API that allows user to 
- [POST] Have a continuous conversation with Cohere's Generative API
- [PUT] Update prompt
- [GET] Check current prompt 

[API Documentation](https://chatbot-47pss7s7fq-de.a.run.app/swagger/)

Architecture Diagram is located at the root of the repository
- ArchitectureDiagram.drawio
- ArchitectureDiagram.png

Unit Test Location:
Vext-Interview\vext_interview\chat\tests.py

Whole thing is built and Deployed with GitLab CI, Cloud Build, and Cloud Run
- Details in .gitlab-ci.yml and cloudbuild.yaml

Just in case:
Here's the Cloud Run URL: https://chatbot-47pss7s7fq-de.a.run.app/api/chat/
