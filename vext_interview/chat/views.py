from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import UserQuestionSerializer, UserPromptSerializer

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from llm.llm_chain import LLModel


llm_chain = LLModel()


class ChatBotView(APIView):
    @swagger_auto_schema(
        operation_id="chat",
        operation_summary="Send chat bot a message",
        operation_description="Start or continue conversation with chatbot. There is only one chat instance.",
        request_body=UserQuestionSerializer,
    )
    def post(self, request, *args, **kwargs):
        serializer = UserQuestionSerializer(data=request.data)

        if serializer.is_valid():
            try:
                response_text = llm_chain.conversation(serializer.data)["text"]
                return Response(response_text, status=status.HTTP_200_OK)
            except Exception as e:
                return Response(
                    {"error": f"Error: {str(e)}"},
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR,
                )
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(
        operation_id="update_prompt",
        operation_summary="Update chat bot prompt",
        operation_description="Update chat bot prompt, it overwrites the existing prompt.",
        request_body=UserPromptSerializer,
    )
    def put(self, request, *args, **kwargs):
        new_prompt_serializer = UserPromptSerializer(data=request.data)

        if new_prompt_serializer.is_valid():
            new_prompt = new_prompt_serializer.validated_data.get("prompt")
            llm_chain.update_prompt(new_prompt)
            return Response(
                status=status.HTTP_204_NO_CONTENT,
            )
        else:
            return Response(
                new_prompt_serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

    @swagger_auto_schema(
        operation_id="get_prompt",
        operation_summary="Get current prompt",
        operation_description="Get the current prompt being used by the chat bot.",
    )
    def get(self, request, *args, **kwargs):
        current_prompt = llm_chain.get_current_prompt()
        if current_prompt is not None:
            return Response(
                {"current_prompt": current_prompt}, status=status.HTTP_200_OK
            )
        else:
            return Response(
                {"error": "Current prompt somehow not available"},
                status=status.HTTP_404_NOT_FOUND,
            )
