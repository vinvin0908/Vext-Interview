from django.db import models


# Create your models here.
class UserQuestion(models.Model):
    question = models.TextField()

    def __str__(self):
        return self.question


class UserPrompt(models.Model):
    prompt = models.TextField()

    def __str__(self):
        return self.prompt
