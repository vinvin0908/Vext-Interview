from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse

from llm.llm_chain import default_prompt


# Create your tests here.
class ChatBotGetCases(APITestCase):
    def test_success(self):
        response = self.client.get(reverse("chat_bot"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["current_prompt"], default_prompt)


class ChatBotPutCases(APITestCase):
    def setUp(self):
        self.test_prompt = "You're a car sales."
        self.test_body = {"prompt": self.test_prompt}
        self.wrong_prompt = "Wrong prompt here."
        self.wrong_body = {"p": self.wrong_prompt}
        self.ignore_body = {"prompt": self.test_prompt, "p": self.wrong_prompt}

    def test_success(self):
        response = self.client.put(reverse("chat_bot"), self.test_body)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        response = self.client.get(reverse("chat_bot"))
        self.assertEqual(response.data["current_prompt"], self.test_prompt)

    def test_fail(self):
        response = self.client.get(reverse("chat_bot"))
        self.assertEqual(response.data["current_prompt"], default_prompt)

        response = self.client.put(reverse("chat_bot"), self.wrong_body)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.get(reverse("chat_bot"))
        self.assertEqual(response.data["current_prompt"], default_prompt)

    def test_ignore(self):
        response = self.client.get(reverse("chat_bot"))
        self.assertEqual(response.data["current_prompt"], default_prompt)

        response = self.client.put(reverse("chat_bot"), self.ignore_body)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        response = self.client.get(reverse("chat_bot"))
        self.assertEqual(response.data["current_prompt"], self.test_prompt)


class ChatBotPostCases(APITestCase):
    def test_success(self):
        correct_body = {"question": "What is a volcano?"}
        response = self.client.post(reverse("chat_bot"), correct_body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_fail(self):
        wrong_body = {"q": "This shouldn't go through."}
        response = self.client.post(reverse("chat_bot"), wrong_body)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_ignore(self):
        excess_body = {
            "question": "What is a volcano?",
            "q": "This shouldn't go through.",
        }  # The extra field will be ignored and return OK
        response = self.client.post(reverse("chat_bot"), excess_body)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
