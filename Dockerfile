FROM python:3.8

EXPOSE 8000
ENV HOST 0.0.0.0

COPY ./vext_interview /vext_interview
COPY requirements.txt requirements.txt

RUN pip install --upgrade pip
RUN pip install --upgrade setuptools
RUN pip install -r requirements.txt

ENTRYPOINT ["python", "/vext_interview/manage.py"]
CMD ["runserver", "0.0.0.0:8000"]